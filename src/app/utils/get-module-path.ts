export function getModulePath(moduleFolder, moduleName) {
  return `./modules/${moduleFolder}/${moduleFolder}.module#${moduleName}`;
}
