import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const ROOT_PATH = '';

const routes: Routes = [
  {
    path: ROOT_PATH,
    loadChildren: './modules/main/main.module#MainModule'
  },
  {
    path: 'main',
    redirectTo: '/',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
