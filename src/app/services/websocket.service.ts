import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { Subject } from 'rxjs/internal/Subject';



export interface IAuthData {
  action: string;
  data: {
    key: number;
    wlcompany: string;
  };
  key: string;
  type: string;
}
export interface IObtainedData {
  data: {
    [propName: string]: any;
  };
  key: string;
}
export enum CONNECTION_STATUS {
  DONE = 200,
}

@Injectable()

export class WebsocketService {

  subject: WebSocketSubject<{}> = webSocket(environment.API_URL);

  _isAuth: boolean;

  statusConnection = new Subject<boolean>();
  obtainedData = new Subject<IObtainedData>();


  constructor() {}

  get isAuth(): boolean {
    return this._isAuth;
  }

  set isAuth(state: boolean) {
    this._isAuth = state;
    this.statusConnection.next(this._isAuth);
  }

  dataProcessing(response): void {
    if (response) {
      const STATE = response.status === CONNECTION_STATUS.DONE;
      if (STATE !== this._isAuth) {
        this.isAuth = STATE;
      }
      this.obtainedData.next({
        data: response.data,
        key: response.key
      });
    }
  }

}
