import { Component, OnInit, OnDestroy } from '@angular/core';
import { WebsocketService, IAuthData } from 'src/app/services/websocket.service';
import { debounceTime, filter, takeWhile, distinctUntilChanged, tap } from 'rxjs/internal/operators';
import { addMonths, addDays, getTime } from 'date-fns';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.less']
})
export class MainComponent implements OnInit, OnDestroy {
  isComponentPresent: boolean;
  key = '58fd27fa-4f81-4e67-a6bf-0e0e3fe4d876';

  defaultReq = {
    action: 'accommodation',
    data: {
      place: {
        in: 'CI005575LO'
      },
      date: {
        in: getTime(addMonths(Date.now(), 1)),
        out: getTime(addDays(addMonths(Date.now(), 1), 2)),
      },
      families: [
        {
          adults: 2
        }
      ],
      lastid: 0,
      num: 5
    },
    type: 'service',
  };

  advertList: {}[];
  currentHash: string;
  backendWait: boolean;

  AUTH_DATA: IAuthData = {
    action: 'login',
    data: {
      key: 123123,
      wlcompany: 'CMPN223463HE'
    },
    key: '4bd97223-9ad0-4261-821d-3e9ffc356e32',
    type: 'account'
  };

  constructor(
    private wsService: WebsocketService,
  ) {
    this.advertList = [];
    this.backendWait = true;
  }

  ngOnInit() {
    this.isComponentPresent = true;
    this.wsService.subject
    .pipe(
      takeWhile(() => this.isComponentPresent),
    )
    .subscribe(
      msg => this.wsService.dataProcessing(msg),  // Called whenever there is a message from the server.
      err => console.log(err),                    // Called if at any point WebSocket API signals some kind of error.
      () => console.log('complete')               // Called when connection is closed (for whatever reason).
    );
    this.wsService.subject.next(this.AUTH_DATA);

    this.wsService.statusConnection
    .pipe(
      takeWhile(() => this.isComponentPresent)
    )
    .subscribe((status: boolean) => {
      if (status) {
        this.getData({...this.defaultReq, key: this.key});
      }
    });

    this.wsService.obtainedData
    .pipe(
      filter((result) => result.key === this.key),
      distinctUntilChanged(),
      debounceTime(2000),
      tap((res) => {
        if (!res.data.done) {
          this.getData({...this.defaultReq, key: this.key});
        } else {
          this.backendWait = true;
        }
      }),
      takeWhile(() => this.isComponentPresent)
    )
    .subscribe(({data, key}) => {
      if (data) {
        this.backendWait = false;
        if (data.hash !== this.currentHash) {
          this.advertList = data.search;
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.isComponentPresent = false;
  }

  getData(data: object): void {
    this.wsService.subject.next(data);
  }

}
