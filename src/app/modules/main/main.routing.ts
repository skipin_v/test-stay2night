import { Routes, RouterModule } from '@angular/router';

import { ModuleWithProviders } from '@angular/core';
import { MainComponent } from './main.component';

const routes: Routes = [
  { path: '',
    children: [
      {
        path: '',
        component: MainComponent
      },
    ],
  },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
