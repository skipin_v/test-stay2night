import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebsocketService } from 'src/app/services/websocket.service';
import { MainComponent } from './main.component';
import { routing } from './main.routing';
import { AdvertComponent } from './advert/advert.component';
import { RatingComponent } from './rating/rating.component';


@NgModule({
  declarations: [
    MainComponent,
    AdvertComponent,
    RatingComponent,
  ],
  imports: [
    CommonModule,
    routing,
  ],
  providers: [
    WebsocketService,
  ]
})
export class MainModule {}
