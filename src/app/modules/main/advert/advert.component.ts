import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-advert',
  templateUrl: './advert.component.html',
  styleUrls: ['./advert.component.less']
})
export class AdvertComponent implements OnInit {

  @Input() advert: {};

  constructor() { }

  ngOnInit() {
  }

  seeOffer(): void {
    alert('Этого в задании не было 😊');
  }

}
