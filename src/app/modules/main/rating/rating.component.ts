import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.less']
})
export class RatingComponent implements OnInit {

  starts: string[];

  @Input() rating: number;

  constructor() {}

  ngOnInit() {
    this.starts = new Array(3 + this.rating).fill('');
  }

}
